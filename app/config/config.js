var angular = angular.module("NodeAngularJSRouting", ["ngRoute"]);

angular.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/sub_one", {
            templateUrl: "/public/view/sub_view_one/list.html",
            controller: "SubViewOneCtrl" })
        .when("/sub_two", {
            templateUrl: "/public/view/sub_view_two/list.html" })
        .otherwise({
            templateUrl: "/public/view/default/default.html",
            controller: "DefaultCtrl" });

    $locationProvider.html5Mode(true);
});