var port = 3000;
var express = require("express");
var morgan = require("morgan");
var server = express();

server.use(morgan("tiny"));

// serve bower components statically
server.use("/bower_components", express.static(__dirname + "/bower_components"));


// for all public requests try to use /app folder
server.use("/public", express.static(__dirname + "/app"));

// for all routes defined on client side send
// entry point to the app
// btw. changes of those routes on client side
// are not supposed to hit server at all (!)
server.use("/", function(req, res, next){
    res.sendFile(__dirname + '/app/main.html');
});

server.listen(port, function() {
    console.log("Node server initialized. Server's port: " + port);
});